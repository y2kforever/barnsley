#! /usr/bin/env python

import random
import matplotlib.pyplot as plt
# starting values
x = 0.5
y = 0.0
# Lists of coordinates to plot
xvec = [x]
yvec = [y]
for i in range(10000):
        rand = random.uniform(0, 1)
        if (rand < 0.02):
                x = 0.5
                y = 0.27*y
        if  ((0.02 <= rand) and (rand <= 0.17)):
                x = -0.139*x + 0.263*y + 0.57
                y = 0.246*x + 0.224*y - 0.036
        if  ((0.17 < rand) and (rand <= 0.3)):
                x = 0.17*x - 0.215*y + 0.408
                y = 0.222*x + 0.176*y + 0.0893
        if ((0.3 < rand) and (rand < 1.0)):
                x = 0.781*x + 0.034*y + 0.1075
                y = -0.032*x +0.739*y + 0.27
        xvec.append(x)
        yvec.append(y)
plt.plot(xvec, yvec, '.')
plt.show()